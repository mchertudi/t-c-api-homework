package API.core;

import com.sendgrid.*;
import java.io.IOException;

/**
 * Created by Matthew on 6/10/2017.
 */
public class SendgridRemailer {
    private static SendGrid sendgrid = new SendGrid(
            "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");

    public static boolean sendMail(String recipient, String title, String body) throws IOException {
        Email from = new Email("Matthew.Chertudi@gmail.com");
        String subject = title+" From SENDGRID";
        Email to = new Email(recipient);
        Content content = new Content("text/plain", body);
        Mail mail = new Mail(from, subject, to, content);

        Request request = new Request();
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            Response response = sendgrid.api(request);
            System.out.println(response.getStatusCode());
            System.out.println(response.getBody());
            System.out.println(response.getHeaders());
        } catch (IOException ex) {
            return false;
        }
        return true;
    }
}
