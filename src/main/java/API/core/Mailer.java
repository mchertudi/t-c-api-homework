package API.core;

import API.email.EmailRepository;
import API.email.MyEmail;
import com.sendgrid.Email;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


import java.io.IOException;


/**
 * Created by Matthew on 6/8/2017.
 */

@Controller
public class Mailer {
    private String recipient;
    private String title;
    private String body;

    private final EmailRepository emailRepository;


    @Autowired
    public Mailer(EmailRepository emailRepository) {
        this.emailRepository = emailRepository;
    }

    @RequestMapping("/mailer")
    @ResponseBody
    public String testEmailer(@ModelAttribute MyEmail myEmail) throws IOException {
        this.recipient = myEmail.getRecipient();
        this.title = myEmail.getTitle();
        this.body = myEmail.getBody();
        emailRepository.save(myEmail);
        SendgridRemailer.sendMail(recipient, title, body);
        AmazonRemailer.sendMail(recipient, title, body);
        return "Recipient: "+recipient+",\nTitle: "+title+",\nBody: "+body;
    }







}
