package API.core;

import API.email.EmailRepository;
import API.email.MyEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Matthew on 6/8/2017.
 */

@Component
public class TestDataLoader implements ApplicationRunner{
    private final EmailRepository emails;

    @Autowired
    public TestDataLoader(EmailRepository emails){
        this.emails = emails;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        List<MyEmail> testMyEmails = Arrays.asList(
                new MyEmail("matthew@megachertudi.net", "To whom it may concern", "Lorem ipsum....."),
                new MyEmail("matthew@megachertudi.net", "To whom it may concern", "Lorem ipsum....."),
                new MyEmail("matthew@megachertudi.net", "To whom it may concern", "Lorem ipsum....."),
                new MyEmail("matthew@megachertudi.net", "To whom it may concern", "Lorem ipsum....."),
                new MyEmail("matthew@megachertudi.net", "To whom it may concern", "Lorem ipsum....."),
                new MyEmail("matthew@megachertudi.net", "To whom it may concern", "Lorem ipsum....."),
                new MyEmail("matthew@megachertudi.net", "To whom it may concern", "Lorem ipsum....."),
                new MyEmail("matthew@megachertudi.net", "To whom it may concern", "Lorem ipsum....."),
                new MyEmail("matthew@megachertudi.net", "To whom it may concern", "Lorem ipsum....."),
                new MyEmail("matthew@megachertudi.net", "To whom it may concern", "Lorem ipsum....."),
                new MyEmail("matthew@megachertudi.net", "To whom it may concern", "Lorem ipsum.....")
        );
        emails.save(testMyEmails);
    }
}
