package API.email;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by Matthew on 6/8/2017.
 */

@Entity
public class MyEmail {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private final Long id;
    private String recipient;
    private String title;
    private String body;

    protected MyEmail(){
        id = null;
    }

    public MyEmail(String recipient, String title, String body) {
        this();
        this.recipient = recipient;
        this.title = title;
        this.body = body;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
