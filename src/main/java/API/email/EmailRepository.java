package API.email;

import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by Matthew on 6/8/2017.
 */

public interface EmailRepository extends PagingAndSortingRepository<MyEmail, Long> {
}
